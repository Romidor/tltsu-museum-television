Дезигн: https://www.figma.com/file/2A66OV1aGKcCSZDZg7sD8i/%D0%9F%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B8-%D0%93%D0%AD%D0%A1-%D0%9C%D1%83%D0%B7%D0%B5%D0%B9?node-id=0%3A1
Таски: https://trello.com/invite/b/42fHL8BI/1c7a2cfa26c72b65e1af3d655491e29a/agile-board

Как делать таски
1. Выбрать задачу.
2. Перенести ее в раздел In progress.
3. Создать ветку. Название ветки должно отражать суть того, что в ней будет сделано.
4. Закодить свою таску.
5. Запушить свои изменения, создать Merge Request (MR) из своей ветки в dev.

Clone repo and марш работать.